<?php

namespace controllers;

use models,
    components;

/**
 * Calling actions
 */
class GlobalController
{
    private $articlesLimited = 5;

    /**
     * Calling the main page
     */
    public function actionIndex()
    {
        $categoryList = models\Category::getList();
        components\View::render('mainpage', $categoryList);
    }

    /**
     * Getting a list of articles
     * @param type $params string
     */
    public function actionArticleList($category)
    {
        $articleList = models\Article::getList($category, $this->articlesLimited);
        if ($articleList === []) {
            echo json_encode('end');
        } else {
            echo (json_encode($articleList));
        }
    }
}
