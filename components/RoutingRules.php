<?php

namespace components;

use controllers;

class RoutingRules
{
    /**
     * Calling the desired action
     */
    public function run()
    {
        $router = new controllers\GlobalController();
        if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') !== 'POST') {
            $router->actionIndex();
        } else {
            $category = array_shift(filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING));
            $router->actionArticleList($category);
        }
    }
}
