<?php

namespace components;

/**
 * Calling page
 */
class View
{
    /**
     * Calling the requested page
     * @param type $files string
     * @param type $data array
     */
    public static function render($file, $categoryList)
    {
        require_once(DIR_VIEW . $file . '.php');
    }
}
