<?php

/**
 * Parameters for connecting to the database
 */
return [
    'host' => 'localhost',
    'dbname' => 'db',
    'user' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
