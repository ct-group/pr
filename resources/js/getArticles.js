$(document).ready(function () {
    /**
     * Sending a request to get the latest articles
     */
    $('.get-articles').on('click', function (e) {
        e.preventDefault();
        $('.content').empty();
        var cid = $(this).val();
        $.ajax({
            url: 'api-url',
            method: 'POST',
            dataType: 'json',
            data: { 'category': cid },
            success: function (response) {
                if (response === 'end') {
                    endArticles();
                    return;
                }
                beginArticles(response);
            },
            error: function () {
                printArticlesError();
            }
        });
        return true;
    });
    var beginArticles = function (data) {
        $.each(data, function (key, value) {
            $('.content').append('<div class="d-flex justify-content-between mb-1"><span class="col bg-secondary">' + value.article_title + '</span><span class="col bg-info">' + value.article_description + '</span><span class="col bg-secondary">' + value.article_image + '</span></div>');
        });
    };
    var endArticles = function () {
        $('.content').append('<p>Нет статей в заданной категории</p>');
    };
    var printArticlesError = function () {
        $('.content').append('<p>Сервер не доступен!</p>');
    };
});