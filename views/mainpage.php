<!DOCTYPE html>
<html lang="ru">

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title></title>
    <link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>

<body class="container-fluid">
    <header class="container text-center">
        <?php
        foreach ($categoryList as $value) { ?>
            <button type="button" value="<?= $value['id']; ?>" class="get-articles btn btn-primary m-5"><?= $value['category_description']; ?></button>
        <?php } ?>
    </header>
    <div class="container text-center content">

    </div>
    <footer></footer>
    <script src="../resources/js/jquery-3.5.1.min.js" type="text/javascript"></script>
    <script src="../resources/js/getArticles.js" type="text/javascript"></script>
</body>

</html>