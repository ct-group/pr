<?php

namespace models;

use components,
    PDO;

/**
 * List of articles
 */
class Article
{
    /**
     * Getting a list of articles
     * @return array
     */
    public static function getList($category, $limited)
    {
        $db = components\DataBase::getConnection();
        $stmt = $db->prepare('
                SELECT	articles.article_title, articles.article_description, articles.article_image
                FROM articles
                LEFT JOIN categories ON categories.id = articles.category_id
                WHERE categories.id = :category ORDER BY articles.id DESC LIMIT :limited
            ');
        $stmt->bindParam(':category', $category);
        $stmt->bindParam(':limited', $limited, PDO::PARAM_INT);
        $stmt->execute();
        $articleList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $articleList;
    }
}
