<?php

namespace models;

use components,
    PDO;

/**
 * List of category
 */
class Category
{
    /**
     * Getting a list of articles
     * @return array
     */
    public static function getList()
    {
        $db = components\DataBase::getConnection();
        $stmt = $db->prepare(
            'SELECT `id`, `category_description` FROM `categories`'
        );
        $stmt->execute();
        $categoryList = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $categoryList;
    }
}
