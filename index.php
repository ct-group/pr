<?php

/**
 * Displaying errors during development
 */
ini_set('display_errors', E_ALL);

/**
 * File connection
 */
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('DIR_VIEW', ROOT . '/views/');
spl_autoload_register('autoload');

function autoload($name)
{
    require_once ROOT . '/' . strtolower($name) . '.php';
}

/**
 * Router call
 */
$router = new components\RoutingRules();
$router->run();
